import { Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { HomeComponent} from '../home/home.component';
import { LogoutComponent } from '../logout/logout.component';
import  {DashboardComponent} from '../dashboard/dashboard.component';
import {TickerTableComponent} from '../ticker-table/ticker-table.component';
import {ChartComponent} from '../chart/chart.component'
import {TradeHistoryComponent} from '../trade-history/trade-history.component'

export const routes: Routes = [
  { path: 'home',  component: HomeComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'logout',   component:LogoutComponent},
  { path: 'dashboard',   component:DashboardComponent},
  { path: 'tickertable',   component:TickerTableComponent},
  { path: 'tradeHistory',   component:TradeHistoryComponent},
  {path: 'chart', component:ChartComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];